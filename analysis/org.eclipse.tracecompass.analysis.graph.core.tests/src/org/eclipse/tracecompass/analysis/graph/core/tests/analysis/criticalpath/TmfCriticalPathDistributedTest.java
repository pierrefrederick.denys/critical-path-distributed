/*******************************************************************************
 * Copyright (c) 2015 École Polytechnique de Montréal
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.tracecompass.analysis.graph.core.tests.analysis.criticalpath;

/**
 * Abstract class to test the critical path algorithms
 *
 * @author Geneviève Bastien
 * @author Francis Giraldeau
 */

public abstract class TmfCriticalPathDistributedTest {

    /**
     *
     */
    public static void main() {
        String str = "PFD";
        System.out.print(str+"\n");
    }

}
